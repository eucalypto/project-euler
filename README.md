# Project Euler

This is my place to play with the Project Euler problems.

## Why?

  I like working on the problems of Project Euler. But usually I take breaks from it for months. This is where a documentation is valuable. I have been using Dynalist as my main note taking app and have a project there. It works decent but I wanted to play with the project abilities of GitLab.

  I recnetly learned that people use issues almost as threads in a forum. This is particularly useful because now I can document my thoughts and don't have to put them into the git repository.